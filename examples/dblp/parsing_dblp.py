#! /usr/bin/python3
import json
from collections import Counter
from lxml.etree import iterparse
import sys
if len(sys.argv)>1:
	path = sys.argv[1]+"/"
else:
	path = "dblp/"
dbpath = "{}/dblp.db".format(path)
tree = iterparse(path+"dblp.xml", load_dtd=True)
tags = set(["article","inproceedings","proceedings","book","incollection","phdthesis","masters	thesis","www","person","data"])
tokeep = set(["article","inproceedings","proceedings","book","incollection", "phdthesis"])
def to_dict(element):
	d = {}
	if element.attrib:
		d["_attrib"] = dict(element.attrib)
	el_tags = Counter(map(lambda e:e.tag, element))
	for x in element:
		if el_tags[x.tag] > 1:
			L = d.get(x.tag, [])
			L.append(to_dict(x))
			d[x.tag] = L
		else:
			d[x.tag] = to_dict(x)
	if not d:
		return element.text
	if element.text:
		text = element.text.strip()
		if text:
			d["_text"] = text
	return d

nodes = open(path+"nodes","w") # nodes file, one JSON by line
edges = open(path+"edges","w") # edges file, node index pairs tabulated
i = 0 # node counter
authors = {} # author dictionnary
for _,el in tree:
	if el.tag not in tags:
		continue
	if el.tag in tokeep:
		d = to_dict(el)
		if "year" in d:
			try:
				d["year"] = int(d["year"])
			except:
				pass
		paper_author = d.pop("author", [])
		if isinstance(paper_author, str):
			paper_author = [paper_author]
		paper_idx = i
		d["index"] = paper_idx
		nodes.write(json.dumps(d)+"\n")
		i += 1
		indices = []
		for author in paper_author:
			if isinstance(author, str):
				name = author
				d = authors[name] = authors.get(name, {})
				if not d:
					d["name"] = author
					d["index"] = i
					i += 1
					#nodes.write(json.dumps(d)+"\n")
			else:
				name = author.pop("_text")
				if not name in authors:
					d = authors[name] = {"name":name, "index":i}
					d.update(author)
					i += 1
					#nodes.write(json.dumps(d)+"\n")
				else:
					d = authors[name]
				for k,v in author.items():
					if not k in d:
						d[k] = v
					elif isinstance(d[k], list):
						d[k].append(v)
					elif not d[k] == v:
						d[k] = [d[k], v]
			indices.append(d["index"])
		edges.write("\n".join(["{}\t{}".format(paper_idx, i) for i in indices])+"\n")
	el.clear()
edges.close()
print("adding authors...")
for author in authors.values():
	nodes.write(json.dumps(author)+"\n")
nodes.close()
