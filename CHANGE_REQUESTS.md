+ add attribute `with_data=dict` to `add_edge` and `add_node`, so that a dictionary with non `str`-keys could be add as edge/node data.
+ make helper accept db connector (not only paths) (try: `nd.sqlite.Graph(db=sqlite3.connect(':memory:'))`)
