Defining Graph Schema
=====================

A Graph Schema is the core of the `ORM`, that is, the mapping
between the recursive dictionary holding the graph data
and the database.

There exist many ways of materializing such a mapping and
NetworkDisk provides some already defined schemas.

The core of those schema are tupledict, that is, a mapping
from a recursive dictionary to a dictionary of tuples.


