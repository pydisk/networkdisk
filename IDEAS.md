# Some not-implemented ideas

## Good

## Not discussed
+	Define an interface between SQL and TupleDict's, with the following methods:
	```python
	def get_rows(self, key_prefix=tuple(), start=None, end=None, distinct=True, limit=None,\
							not_starting_with_None=True, not_ending_with_None=False, ordered=False):
		pass
	def del_rows(self, kpref):
		pass
	def count_rows(self, key_prefix=tuple(), start=None, end=None, distinct=True, limit=None):
		pass
	def exist_row(self, key_prefix=tuple(), distinct=True, limit=None):
		pass
	def pop_rows(self, kpref, start=None, end=None, distinct=True, limit=None, ordered=False):
		pass
	def insert_rows(self, key_prefix=tuple(), on_conflict_replace=True):
		pass
	```
	The interface could actually even be a replacement to TupleDict's…
+	Cache management. See [](https://nikos7am.com/posts/mutable-default-arguments/).

## Rejected
+	Table keys 
