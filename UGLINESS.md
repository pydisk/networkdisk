
## Networkx
+	`networkx.Graph.nbunch_iter`
	accepts an iterator or a node
	+	in first case, returns the intersection of `Graph._node` and the given `iterator`;
	+	in second case, returns the node if it belongs to `Graph._node` or raises an error

+	`networkx.Graph.nbunch_iter`
	do not raise at call, but at iteration of result.
	We do not follow this semantic, and we directly raise at call.
	> G = nd.sqlite.DiGraph()
	> G.nbunch_iter(-1)
	!NetworkDiskError…
	> G.nbunch_iter([1, 2, {}])
	!NetworkDiskError…

+	`networkx.classes.reportviews.OutEdgeDataView`
	ignores `nbunch` (selected nodes) on `__contains__`
	```python
	nxG = nx.DiGraph(incoming_graph_data=enumerate(range(1, 4)))
	r = nxG.out_edges(range(1))
	assert (3, 4) in r
	assert (3, 4) not in list(r)
	assert (4, 3) not in list(r)
	```

+	tests (test_graph_attr and test_update)
	```python
	assert G.graph == {}
	```
	rather than
	```python
	assert G.graph == G.graph_attr_dict_factory()
	```

+	temporary graph creation
	In some algorithms of networkx, some intemediate graph class
	are used. They are infer using __class__ attributes but it would
	be nicer to have a method temporary_graph_class to allow a
	more precise control of the type of graph created.
	Example:
	```python
    edges = minimum_spanning_edges(
        G, algorithm, weight, keys=True, data=True, ignore_nan=ignore_nan
    )
    T = G.__class__()  # Same graph class as G
    T.graph.update(G.graph)
    T.add_nodes_from(G.nodes.items())
    T.add_edges_from(edges)
	```
	In `minimum_spanning_tree` algorithms.

## SQLite
INNER JOIN ON clause seems to be translated to WHERE clause, leading to invalid behaviors:
```sqlite
CREATE TABLE tab1(col1); CREATE TABLE tab2(col2); CREATE TABLE tab3(col3);
SELECT * FROM tab1 INNER JOIN tab2 ON col3 LEFT JOIN tab3;
```
