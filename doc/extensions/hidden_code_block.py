"""Simple, inelegant Sphinx extension which adds a directive for a
highlighted code-block that may be toggled hidden and shown in HTML.
This is possibly useful for teaching courses.

The directive, like the standard code-block directive, takes
a language argument and an optional linenos parameter.
The hidden-code-block adds label as optional parameter.

Code snippet extracted and adapted from https://github.com/scopatz/hiddencode

Examples:

.. hidden-code-block:: python
    a = 10
    b = a + 5

.. hidden-code-block:: python
    :label: --- SHOW/HIDE ---

    x = 10
    y = x + 5

Written by Anthony 'el Scopz' Scopatz, January 2012.
Adapted by "Charles Paperman", April 2021

Adaptation use the html tags
<summary> / <details> to prevent the use of JavaScript.

Released under the WTFPL (http://sam.zoy.org/wtfpl/).
"""

from docutils import nodes
from docutils.parsers.rst import directives
from sphinx.directives.code import CodeBlock

class hidden_code_block(nodes.General, nodes.FixedTextElement):
    pass

class HiddenCodeBlock(CodeBlock):
    """Hidden code block is Hidden"""

    option_spec = dict(label=str,
                       **CodeBlock.option_spec)

    def run(self):
        # Body of the method is more or less copied from CodeBlock
        code = u'\n'.join(self.content)
        hcb = hidden_code_block(code, code)
        hcb['language'] = self.arguments[0]
        hcb['linenos'] = 'linenos' in self.options
        hcb['label'] = self.options.get('label', 'show/hide')
        hcb.line = self.lineno
        return [hcb]


def visit_hcb_html(self, node):
    """Visit hidden code block"""

    # We want to use the original highlighter so that we don't
    # have to reimplement it.  However it raises a SkipNode
    # error at the end of the function call.  Thus we intercept
    # it and raise it again later.
    try:
        self.visit_literal_block(node)
    except nodes.SkipNode:
        pass

    # The last element of the body should be the literal code
    # block that was just made.
    code_block = self.body[-1]
    code_block = f"""
	<div class="fold-block">
    <details>
        <summary style="cursor:pointer;color=#2980B9"> {node.get('label', 'SHOW')} </summary>
        {code_block}
    </details>
	</div>
   """
    # reassign and exit
    self.body[-1] = code_block
    raise nodes.SkipNode


def depart_hcb_html(self, node):
    """Depart hidden code block"""
    # Stub because of SkipNode in visit


def setup(app):
    app.add_directive('hidden-code-block', HiddenCodeBlock)
    app.add_node(hidden_code_block, html=(visit_hcb_html, depart_hcb_html))
