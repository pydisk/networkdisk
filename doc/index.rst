.. _contents:

.. include:: ../README.rst

.. only:: html

    :Date: |today|

.. toctree::
   :maxdepth: 2

   install
   tutorials/index
   examples/index
   references/index
   license

Indices
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
