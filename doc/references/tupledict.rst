TupleDict
=========

Introduction
------------

.. currentmodule:: networkdisk.tupledict
.. automodule:: networkdisk.tupledict

Fold and Unfold functions
-------------------------

The operation of converting a recursive-dict into a tuple-dict
and vice versa are called :py:func:`fold` and :py:func:`unfold`.

.. autofunction:: unfold
.. autofunction:: fold

TupleDictView
-------------

.. autoclass:: networkdisk.tupledict.ReadOnlyTupleDictView
.. autoclass:: networkdisk.tupledict.ReadWriteTupleDictView

RowStores
---------

.. autoclass:: networkdisk.tupledict.BaseAbstractRowStore
.. autoclass:: networkdisk.tupledict.ReadWriteAbstractRowStore
 
