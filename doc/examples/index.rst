Examples
========

The package is also provided with several examples of large
graphs. Those Graphs can be downloaded to play with them, or they can be generated
using the provided script.

Caution: the time of generation of those graphs can be prohibitive.

.. toctree::
   :maxdepth: 2

   dblp
   wikipedia

