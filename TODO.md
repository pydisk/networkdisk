+ Continuous integration: autotest on push with multiple networkx versions using virtualenv
+ QueryBuilder: define API, and move files to subpackage
+ QueryBuilder: add pretty_print methods for SQL logging
+ Documentation: complete doctest and unitexamples
+ Documentation: tutorials
+ Documentation: discussion pages on specific components such as tupleDicts
+ Schema: postgresql implementation
+ Schema: schema for data free graphs
+ Schema: schema for edge-data free graphs
+ Uniformize EdgeView ⟶ NodeView

+ sql.tupledict.ReadWriteRowStore: bulk_insert supporting iterable_query ⇒ check rowstore semantics

+ make SQL-version of fold
	```SQL
	SELECT name, target, json_group_object(key, value)
	FROM nodes1
	LEFT JOIN symedges1 ON name = source
	LEFT JOIN edge_data1 ON symedges1.id = edge_data1.id
	GROUP BY name, target
	```
	then followed by a decoding of mapping third coordinates on python-side (map).
  ⇒ override TupleDictView items

+ add a high level operation taking a set of nodes/iq and returning an iq with their neighbors.
